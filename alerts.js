var api = "https://api.weather.gov/alerts/active?area="
var states = ["AL","AK", "AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID", "IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO", "MT", "NE","NV","NH","NJ", "NM","NY","NC", "ND", "OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY",]

for (let i = 0; i < states.length; i++) {
    let state = states[i];
    addStateButton(state, $("#states" + ((i % 5) + 1)), i % 2 == 0);
}

function addStateButton(state, colToAddTo, color) {
    var button = $("<button class=\"btn btn-secondary btn-block\" data-toggle=\"collapse\" href=\"#warning\">" + state + "</button>").appendTo(colToAddTo);
    if (color) button.addClass("bg-dark")
    $.ajax({
        url: api + state,
        success: function(data){
            button.click(function(){
                var s = "";
                var features = data.features;
                for (var i = 0; i < features.length; i++) {
                    var f = features[i];
                    s += "<p>" + f.properties.headline + "</p>";
                }
                if (s == "") s = "No dangers near " + state + " :)))";
                $("#warning").html(s);
            })
        }
    });
}